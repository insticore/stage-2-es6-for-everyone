export function getRandomBetween(min, max) {
    return Math.random() * (max - min) + min;
};