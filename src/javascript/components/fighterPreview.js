import { createElement } from '../helpers/domHelper';
import { fighters } from '../helpers/mockData';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    fighterElement.append(createFighterInfo(fighter));
  }

  return fighterElement;
}

function createFighterInfo(fighter) {
  const fighterInfo = createElement({
    tagName: 'div',
    className: 'fighter-preview___wrapper',
  });


  const imageWrapper = createElement({
    tagName: 'div',
    className: 'fighter-preview___img-wrapper',
  });
  
  fighterInfo.append(createFighterImageWrapper(fighter));
  fighterInfo.append(createFighterFields(fighter));

  return fighterInfo;
}

function createFighterImageWrapper(fighter) {
  const fighterImageWrapper = createElement({
    tagName: 'div',
    className: 'fighter-preview___img-wrapper',
  });

  fighterImageWrapper.append(createFighterImage(fighter));
  return fighterImageWrapper;
}

function createFighterFields(fighter) {
  const fields = createElement({
    tagName: 'div',
    className: 'fighter-preview___fields-wrapper',
  });
  fields.append(createFighterField("Name", fighter.name));
  fields.append(createFighterField("Health", fighter.health));
  fields.append(createFighterField("Attack", fighter.attack));
  fields.append(createFighterField("Defense", fighter.defense));

  return fields;
}

function createFighterField(fieldName, fieldText) {
  const field = createElement({
    tagName: 'div',
    className: 'fighter-preview___field',
  });
  field.innerText = fieldName + ": " + fieldText;

  return field;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
