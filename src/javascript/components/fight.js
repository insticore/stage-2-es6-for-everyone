import { controls } from '../../constants/controls';
import { getRandomBetween } from '../helpers/randomHelper';

import Player from './player';

export async function fight(firstFighter, secondFighter) {
  const leftPlayer = new Player('left');
  const rightPlayer = new Player('right');
  setListeners(leftPlayer, rightPlayer);

  return new Promise((resolve) => {
    leftPlayer.setPlayer(firstFighter, resolve, getHealthBar(leftPlayer.position));
    rightPlayer.setPlayer(secondFighter, resolve, getHealthBar(rightPlayer.position));
  });
}

function getHealthBar(position) {
  return document.getElementById(`${position}-fighter-indicator`);
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage < 0) {
    return 0;
  } else {
    return damage;
  }
}

export function getHitPower(fighter) {
  return fighter.attack * getCriticalHitChance();
}

export function getBlockPower(fighter) {
  return fighter.defense * getDodgeChance();
}

function setListeners(leftPlayer, rightPlayer) {
  document.addEventListener('keydown', function (event) {
    switch (event.code) {
      case controls.PlayerOneAttack:
        leftPlayer.attackPressed = true;
        attack(leftPlayer, rightPlayer);
        break;
      case controls.PlayerTwoAttack:
        rightPlayer.attackPressed = true;
        attack(rightPlayer, leftPlayer);
        break;
      case controls.PlayerOneBlock:
        leftPlayer.blockPressed = true;
        break;
      case controls.PlayerTwoBlock:
        rightPlayer.blockPressed = true;
        break;
      case controls.PlayerOneCriticalHitCombination[0]:
      case controls.PlayerOneCriticalHitCombination[1]:
      case controls.PlayerOneCriticalHitCombination[2]: {
        const index = controls.PlayerOneCriticalHitCombination.indexOf(event.code);
        leftPlayer.criticalHitPressed[index] = true;
        if (leftPlayer.isCriticalHitting()) {
          attackWithCriticalHit(leftPlayer, rightPlayer);
        }
        break;
      }
      case controls.PlayerTwoCriticalHitCombination[0]:
      case controls.PlayerTwoCriticalHitCombination[1]:
      case controls.PlayerTwoCriticalHitCombination[2]: {
        const index = controls.PlayerTwoCriticalHitCombination.indexOf(event.code);
        rightPlayer.criticalHitPressed[index] = true;
        if (rightPlayer.isCriticalHitting()) {
          attackWithCriticalHit(rightPlayer, leftPlayer);
        }
        break;
      }
    }
  });

  document.addEventListener('keyup', function (event) {
    switch (event.code) {
      case controls.PlayerOneAttack:
        leftPlayer.attackPressed = false;
        break;
      case controls.PlayerTwoAttack:
        rightPlayer.attackPressed = false;
        break;
      case controls.PlayerOneBlock:
        leftPlayer.blockPressed = false;
        break;
      case controls.PlayerTwoBlock:
        rightPlayer.blockPressed = false;
        break;
      case controls.PlayerOneCriticalHitCombination[0]:
      case controls.PlayerOneCriticalHitCombination[1]:
      case controls.PlayerOneCriticalHitCombination[2]: {
        const index = controls.PlayerOneCriticalHitCombination.indexOf(event.code);
        leftPlayer.criticalHitPressed[index] = false;
        break;
      }
      case controls.PlayerTwoCriticalHitCombination[0]:
      case controls.PlayerTwoCriticalHitCombination[1]:
      case controls.PlayerTwoCriticalHitCombination[2]: {
        const index = controls.PlayerTwoCriticalHitCombination.indexOf(event.code);
        rightPlayer.criticalHitPressed[index] = false;
        break;
      }
    }
  });
}

function attack(attackingPlayer, defendingPlayer) {
  if (!attackingPlayer.blockPressed && !defendingPlayer.blockPressed) {
    const damage = getDamage(attackingPlayer.fighter, defendingPlayer.fighter);
    defendingPlayer.handleDamage(damage);
    if (!defendingPlayer.isAlive()) {
      attackingPlayer.win();
    }
  }
}

function attackWithCriticalHit(attackingPlayer, defendingPlayer) {
  if (!attackingPlayer.blockPressed) {
    const damage = attackingPlayer.getCriticalHitDamage();
    defendingPlayer.handleDamage(damage);
    if (!defendingPlayer.isAlive()) {
      attackingPlayer.win();
    }
  }
}

function getCriticalHitChance() {
  return getRandomBetween(1, 2);
}

function getDodgeChance() {
  return getRandomBetween(1, 2);
}