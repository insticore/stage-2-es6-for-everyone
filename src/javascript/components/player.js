class Player {
    constructor(position) {
        this.position = position;
        this.attackPressed = false;
        this.blockPressed = false;
        this.criticalHitPressed = [false, false, false];
    }

    setPlayer(fighter, resolve, bar) {
        this.fighter = fighter;
        this.initialHealth = fighter.health;
        this.resolve = resolve;
        this.bar = bar;
    }

    win() {
        this.resolve({ name: this.fighter.name, position: this.position });
    }

    isAttacking() {
        return this.attackPressed;
    }

    isBlocking() {
        return this.blockPressed;
    }

    isCriticalHitting() {
        return this.criticalHitPressed.every(v => v);
    }

    handleDamage(damage) {
        if (damage >= this.fighter.health) {
            this.fighter.health = 0;
            this.updateHealthBar();
        } else {
            this.fighter.health -= damage;
            this.updateHealthBar();
        }
    }

    updateHealthBar() {
        if (this.bar) {
            const portion = Math.round(this.fighter.health / this.initialHealth * 100);
            this.bar.style.width = `${portion}%`;
        }
    }

    isAlive() {
        return this.fighter.health > 0;
    }

    getCriticalHitDamage() {
        if (this.criticalHitUnavailable) {
            return 0;
        } else {
            this.criticalHitUnavailable = true;
            setTimeout(() => this.criticalHitUnavailable = false, 10000);
            return 2 * this.fighter.attack;
        }
    }
}

export default Player;