import { createElement } from '../../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(winner) {
  const title = "Fight is over!";
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  bodyElement.innerText = `"Winner is ${winner.name} (${winner.position})`;
  const onClose = () => window.location.href = document.location.protocol + '//' + document.location.host;

  showModal({title: title, bodyElement: bodyElement, onClose: onClose});
}
